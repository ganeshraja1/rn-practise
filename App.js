/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow strict-local
 */
import 'react-native-gesture-handler';
import React from 'react';
import {t} from 'react-native-tailwindcss';
import AppNavigationContainer from './src/navigations';
import GlobalProvider from './src/context/Provider';

const App = () => {
  return (
    <GlobalProvider>
      <AppNavigationContainer></AppNavigationContainer>
    </GlobalProvider>
  );
};

export default App;
