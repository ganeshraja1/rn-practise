import React from 'react'
import { ScrollView, View } from 'react-native'
import {t} from 'react-native-tailwindcss'

const Container = ({children}) => {
    return (
        <ScrollView>
            <View style={[t.p10]}> {children} </View>
        </ScrollView>
    )
}

export default Container
