export const CONTACT_LIST = 'contacts'
export const CONTACT_DETAIL = 'contact details'
export const CREATE_CONTACT = 'create contact'
export const SETTINGS = 'settings'
export const LOGIN = 'login'
export const REGISTER = 'register'
export const HOME_SCREEN_NAVIGATOR = 'home'