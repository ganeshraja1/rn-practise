import React, { createContext, useReducer } from 'react'
import auth from './reducers/auth'

import authInitialState from './initialStates/authInitialState'
import contactInitialState from './initialStates/contactInitialState'
import contacts from './reducers/contacts'

export const GlobalContext = createContext({})

const GlobalProvider = ({ children }) => {
    const [authState, authDispatch] = useReducer(auth, authInitialState)
    const [contactState, contactsDispatch] = useReducer(contacts, contactInitialState)
    return (
        <GlobalContext.Provider value={{authState, authDispatch, contactState, contactsDispatch}}>{ children}</GlobalContext.Provider>
    )
}

export default GlobalProvider