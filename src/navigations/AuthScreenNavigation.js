import React from 'react';
import {createStackNavigator} from '@react-navigation/stack';
import { Text, SafeAreaView } from 'react-native';
import {LOGIN, REGISTER} from '../constants/routeNames'

import {Login, Signup} from '../screens';


const AuthScreenNavigation = () => {
  const Stack = createStackNavigator();
  return (
    <Stack.Navigator initialRouteName={Login}>
      <Stack.Screen name={LOGIN} component={Login} />
      <Stack.Screen name={REGISTER} component={Signup} />
    </Stack.Navigator>
  );
};

export default AuthScreenNavigation;
