import React from 'react'
import {createDrawerNavigator} from '@react-navigation/drawer';
import HomeScreenNavigation from './HomeScreenNavigation';
import AuthScreenNavigation from './AuthScreenNavigation';

import {HOME_SCREEN_NAVIGATOR} from '../constants/routeNames';

const DrawerNavigator = () => {
    const Drawer = createDrawerNavigator();
    return (
      <Drawer.Navigator>
        <Drawer.Screen
          name={HOME_SCREEN_NAVIGATOR}
          component={HomeScreenNavigation}
        />
      </Drawer.Navigator>
    );
}

export default DrawerNavigator