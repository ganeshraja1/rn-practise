import React from 'react'
import { createStackNavigator } from '@react-navigation/stack';
import {Text, SafeAreaView} from 'react-native'
import { CONTACT_DETAIL, SETTINGS, CONTACT_LIST, CREATE_CONTACT } from '../constants/routeNames';


import {Contacts, ContactDetail, CreateContact, Settings} from '../screens'


const HomeScreenNavigation = () => {
    const Stack = createStackNavigator()
    return (
      <Stack.Navigator initialRouteName={Contacts}>
        <Stack.Screen name={CONTACT_LIST} component={Contacts} />
        <Stack.Screen name={CONTACT_DETAIL} component={ContactDetail} />
        <Stack.Screen name={CREATE_CONTACT} component={CreateContact} />
        <Stack.Screen name={SETTINGS} component={Settings} />
      </Stack.Navigator>
    );
}

export default HomeScreenNavigation
