import { NavigationContainer } from '@react-navigation/native'
import React, {useContext} from 'react'
import AuthScreenNavigation from './AuthScreenNavigation';
import DrawerNavigator from './DrawerNavigator'
import {GlobalContext} from '../context/Provider'

import {Text} from 'react-native'

const AppNavigationContainer = () => {
  const {authState:{isLoggedIn}} = useContext(GlobalContext)
    return (
      <NavigationContainer>
        {isLoggedIn ? <DrawerNavigator /> : <AuthScreenNavigation />}
      </NavigationContainer>
    );
}

export default AppNavigationContainer
