export {default as ContactDetail} from './contactdetail';
export {default as Contacts} from './contacts';
export {default as CreateContact} from './createcontact';
export {default as Settings} from './settings';
export {default as Login} from './Login';
export {default as Signup} from './Register';